module tp_sae {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.desktop;
    requires transitive javafx.graphics;
    exports tp_sae;
    exports tp_sae.tp1;
    exports tp_sae.dijkstra;
    exports tp_sae.astar;
    opens tp_sae to javafx.fxml;
    opens tp_sae.tp1 to javafx.fxml;
    opens tp_sae.dijkstra to javafx.fxml;
    opens tp_sae.astar to javafx.fxml;
}