package tp_sae.tp1;

import java.util.List;
import java.util.Random;

import javafx.scene.paint.Color;
import tp_sae.Algorithme;
import tp_sae.Cellule;
import tp_sae.Labyrinthe;

public class Demo extends Algorithme {

    private Random random = new Random();

    public Demo(Labyrinthe labyrinthe) {
        super(labyrinthe);
    }

    @Override
    public void run() {
        Cellule entree = Cellule.getEntree();
        entree.colorier(Color.SALMON);
        Cellule location = entree;
        while (! location.equals(getSortie())) {
            List<Cellule> voisins = location.getVoisins();
            int i = random.nextInt(voisins.size());
            location.colorier(Color.PINK);
            location = voisins.get(i);
            location.colorier(Color.BLUE);
            pause(0, 1);
        }
    }
}
