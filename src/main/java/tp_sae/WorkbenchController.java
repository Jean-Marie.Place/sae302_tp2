package tp_sae;

import javafx.fxml.FXML;
import javafx.scene.layout.Pane;
import tp_sae.astar.AStar;
import tp_sae.dijkstra.Dijkstra;
import tp_sae.tp1.Demo;
import tp_sae.tp1.file.File;
import tp_sae.tp1.pile.Pile;

public class WorkbenchController {
    @FXML
    Pane display;

    Labyrinthe labyrinthe;

    public void initialize() {
        labyrinthe = new Labyrinthe(display);
        display.setPrefWidth(500);
        display.setPrefHeight(615);
    }

    @FXML
    public void dijkstra() {
        labyrinthe = new Labyrinthe(display);
        Dijkstra dijkstra = new Dijkstra(labyrinthe);
        dijkstra.start();
    }

    @FXML
    public void astar() {
        labyrinthe = new Labyrinthe(display);
        AStar astar = new AStar(labyrinthe);
        astar.start();
    }

    @FXML
    public void pile() {
        labyrinthe = new Labyrinthe(display);
        Pile pile = new Pile(labyrinthe);
        pile.start();
    }

    @FXML
    public void file() {
        labyrinthe = new Labyrinthe(display);
        File file = new File(labyrinthe);
        file.start();
    }

    @FXML
    public void demo() {
        labyrinthe = new Labyrinthe(display);
        Demo demo = new Demo(labyrinthe);
        demo.start();
    }
}