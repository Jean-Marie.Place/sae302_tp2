package tp_sae;

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Written by C. Durr, Ecole Polytechnique, INF421
 *
 * Modifs I. Boneva, CRIStAL, Université de Lille
 *   and F. Guyomarch, L2EP, Université de Lille
 */

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

/**
 * Une classe simple pour afficher un labyrinthe stocké dans un tableau de
 * <code>String</code>.
 *
 * @author <a href="mailto:frederic.guyomarch@univ-lille.fr">Frédéric
 *         Guyomarch</a> (ULille)
 * @version 1 sept. 2022
 */
public class Labyrinthe /* extends Canvas */ {
    private String[] murs = {
        "#########################################",
        "    #        # # # #       #        # # #",
        "### # #### # # # # # ### ##### ## ### # #",
        "#   # #    # #   #   #   # #    #   #   #",
        "### ### ###### ######### # ###  # # ### #",
        "#          #   #     #   #        # #   #",
        "### ##### #### # ####### ### ###### ### #",
        "# # # # #  #     #   #       #    #     #",
        "# # # # # ## ### # # # # ###### ##### ###",
        "#   # #        #   #   #   #      #     #",
        "# #        # # # #   # # # # #      # # #",
        "# # # ###### # # ### # ### # # #### ### #",
        "# # #      # #     # #     # #  # #   # #",
        "# # ###### ##### ######### # #  # # ### #",
        "#   #      # #         #   #            #",
        "########## # ##### ######### ## ####### #",
        "#   #          #   # #     #      #     #",
        "### # ### #### ##### ### ######## ##### #",
        "#   #   #            #          # #     #",
        "### #####  ##### ########### #### ##### #",
        "#   #   #  #   #       #            # # #",
        "### # ### ## ### ####### # ######## # # #",
        "#     #    #           # #        # #   #",
        "### ### ######## ########### ## # # ### #",
        "#                # #         #  # # #   #",
        "### ### ###### # # # ########## ### ### #",
        "# # #      # # #   #       # #  #     # #",
        "# ##### # ## ##### # # # # # # ## ### ###",
        "#       #    # # # # # # #          #   #",
        "##### #### # # # # # ########## # #######",
        "#       #  #     #   #     #    # #     #",
        "# # #            # # #   #        # #   #",
        "# # # # ## ########### # # ### ###### ###",
        "# # # #    #   # # #   # # # #  #   #   #",
        "# ### #### # ### # ##### ### ## # # #  ##",
        "#   # #          # # #     #      #     #",
        "# ### ### ###### # # # # #####  # ### ###",
        "# #   #    #     #   # #        # #     #",
        "### ### # ## # ### ###########  ### # # #",
        "#     # #  # #             #      # # #  ",
        "#########################################" 
        };

    // Affichage javaFX    
    static final int c = 15; // taille d'affichage d'une cellule
    private final Shape[][] shapes; // Tableau des formes javaFX pour l affichage.

    // les marques
    private final char[][] marque;

    private final int[] entree;
    private final int[] sortie;

    // cree une fenetre et affiche le labyrinthe dedans
    Labyrinthe(Pane display) {
        marque = new char[size()][size()];
        shapes = new Shape[size()][size()];
        sortie = new int[] { murs.length - 1, murs.length - 2 };
        entree = new int[] { 0, 1};
        for (int x = 0; x < size(); x++) {
            for (int y = 0; y < size(); y++) {
                marque[x][y] = VIDE;
            }
        }
        for (int x = 0; x < size(); x++) {
            for (int y = 0; y < size(); y++) {
                Shape shape;
                double baseX = c*x;
                double baseY = c*y;
                if (estMur(x, y)) {
                    shape = new Rectangle(baseX, baseY, c, c);
                } else {
                    shape = new Circle(baseX+c/2.0, baseY+c/2.0, c/4.0);
                }
                shapes[x][y] = shape;
                display.getChildren().add(shape);
            }
        }
        resetColors();
    }

    // coordonnées (x,y) de l'entree
    public int[] entree() {
        return entree;
    }

    // coordonées (x,y) de la sortie
    public int[] sortie() {
        return sortie;
    }

    // est-ce la sortie ?
    boolean estSortie(int x, int y) {
        return x == sortie[0] && y == sortie[1];
    }

    // est-ce la sortie ?
    public boolean estSortie(int[] s) {
        return s[0] == sortie[0] && s[1] == sortie[1];
    }

    // est-ce que (x,y) est un mur ?
    public boolean estMur(int x, int y) {
        return murs[y].charAt(x) != ' ';
    }

    // est-ce que (x,y) est un mur ?
    public boolean estMur(int[] m) {
        return murs[m[1]].charAt(m[0]) == '#';
    }

    // la taille du labyrinthe, a la fois nb lignes et nb colonnes
    public int size() {
        return murs.length;
    }

    static final char VIDE = 'a', ROUGE = 'b', ROSE = 'c';

    // teste si la cellule (x,y) comporte une marque
    boolean estMarque(int x, int y) {
        return marque[x][y] != VIDE;
    }

    // public boolean estMarque(int[] c) {
    //     return marque[c[0]][c[1]] != VIDE;
    // }

    // pose une marque
    public void poserMarque(int x, int y) {
        marque[x][y] = ROSE;
        shapes[x][y].setFill(Color.PINK);
    }


    // public void poserMarqueChemin(int x, int y) {
    //     marque[x][y] = ROUGE;
    //     shapes[x][y].setFill(Color.RED);
    // }

    // public void poserMarqueChemin(int[] c) {
    //     marque[c[0]][c[1]] = ROUGE;
    //     shapes[c[0]][c[1]].setFill(Color.RED);
    // }

    public void colorier(int x, int y, Paint paint) {
        shapes[x][y].setFill(paint);
    }

    public void resetColors() {
        for (int x = 0; x < size(); x++) {
            for (int y = 0; y < size(); y++) {
                if (estMur(x, y)) {
                    shapes[x][y].setFill(Color.GRAY);
                } else {
                    shapes[x][y].setFill(Color.WHITE);
                }
            }
        }
    }

    public boolean isOk(int x, int y) {
        if (x < 0) return false;
        if (y < 0) return false;
        if (x >= size()) return false;
        if (y >= size()) return false;
        return true;
    }
}
