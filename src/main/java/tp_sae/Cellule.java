package tp_sae;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import javafx.scene.paint.Paint;

public class Cellule {
        private static Cellule[][] cellules;
        private static Labyrinthe labyrinthe;
        private static Cellule entree;
        private static Cellule sortie;
        protected final int x;
        protected final int y;
        private static final Random RAND = new Random();
        private Cellule predecesseur;
        private int distance;
        private Integer heuristique;
        private List<Cellule> voisins;
    
        /**
         * Initialisation des éléments static de la classe.
         * Nécessaire pour utiliser {@link #labyrinthe #getCellule}, {@link #getEntree()} et {@link #getSortie}.
         * @param labyrinthe
         */
        public static void setLabyrinthe(Labyrinthe labyrinthe) {
            Cellule.labyrinthe = labyrinthe;
            Cellule.cellules = new Cellule[labyrinthe.size()][labyrinthe.size()];
            Cellule.entree = Cellule.getCellule(labyrinthe.entree()[0], labyrinthe.entree()[1]);
            Cellule.sortie = Cellule.getCellule(labyrinthe.sortie()[0], labyrinthe.sortie()[1]);
        }

    /**
     * Retourne la cellule du labyrinthe enposition (x, y) (la crée uniquement si elle n'a pas déjà été créée)
     * @param x position x de la cellule voulue.
     * @param y position en y de la cellule voulue.
     * @return la cellule.
     */
    public static Cellule getCellule(int x, int y) {
        if (x < 0 || x > Cellule.labyrinthe.size()) { throw new ArrayIndexOutOfBoundsException("x hors limites (" + x + ")"); }
        if (y < 0 || y > Cellule.labyrinthe.size()) { throw new ArrayIndexOutOfBoundsException("y hors limites (" + y + ")"); }
        if (Cellule.cellules[x][y] == null) {
            Cellule.cellules[x][y] = new Cellule(x, y);
        }
        return Cellule.cellules[x][y];
    }


    /**
     * Retourne la cellule d'entrée du labyrinthe.
     * @return La cellule d'entrée du labyrinthe.
     */
    public static Cellule getEntree() {
        return Cellule.entree;
    }

    /**
     * Retourne la cellule de sortie du labyrinthe.
     * @return La cellule de sortie du labyrinthe.
     */
    public static Cellule getSortie() {
        return Cellule.sortie;
    }

    /**
     * La création de cellule passe uniquement par la méthode static {@link #getCellule(int, int)}
     * @param x
     * @param y
     */
    protected Cellule(int x, int y) {
        this.x = x;
        this.y = y;
        this.voisins = null;
        distance = Integer.MAX_VALUE;
        this.heuristique = null;
    }

    /*
     * Fonctionalités de base: getX(), getY(), getVoisins(), estMur(), poserMarque(), estMarque(), equals, hashCode, toString
     */
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    /**
     * Fournit la liste des cellules voisines. Celles-ci peuvent être des murs mais ne sont pas hors limites. 
     * Utilise la liste des déplacement légaux dans Algorithme.MOVES.
     * @return La liste éventuellement vide des cellules voisines.
     */
    public List<Cellule> getVoisins() {
        assert !this.estMur();
        if (voisins == null) {
            voisins = new ArrayList<>();
            for (Algorithme.Move move : Algorithme.MOVES) {
                int xTarget = move.xTarget(this);
                int yTarget = move.yTarget(this);
                if (labyrinthe.isOk(xTarget, yTarget) && !labyrinthe.estMur(xTarget, yTarget)) {
                        voisins.add(Cellule.getCellule(xTarget, yTarget));                    
                }
            }
        }
        return voisins;
    }

    /**
     * Indique si la cellule courante est un mur.
     * @return true si c'est un mur, false sinon.
     */
    public boolean estMur() {
        return labyrinthe.estMur(x, y);
    }

    /**
     * Marque la cellule courante.
     */
    public void poserMarque() {
        labyrinthe.poserMarque(x, y);
    }

    /**
     * Inidique si la cellule courante a été marquée.
     * @return true si la cellule est marquée, false sinon.
     */
    public boolean estMarque() {
        return labyrinthe.estMarque(x, y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (!(o instanceof Cellule)) { return false; }
        Cellule cellule = (Cellule) o;
        return x == cellule.x && y == cellule.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    public String toString() {
        return "(" + x + ", " + y + ")";
    }

    /*
     * Fonctionnalités d'affichage javaFX (colorier(Paint paint)
     */

    /**
     * Met la cellule selon une des couleurs javaFX (Color.BLACK par exemple)
     * @param paint La couleur de peinture de la case.
     */
    public void colorier(Paint paint) {
        labyrinthe.colorier(x, y, paint);
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getDistance() {
        return this.distance;
    }

    public int getHeuristique() {
        if (this.heuristique == null) {
            heuristique = -Math.abs((x - getSortie().getX()) - Math.abs(y - getSortie().getY()));
        }
        return heuristique;
    }

    /**
     * Choisit une celllule au hasard parmi les cellule voisines non matrquées de la cellule courante.
     * @return La cellule choisie, null si aucun choix possible.
     */
    public Cellule voisineNonMarquee() {
        List<Cellule> nonMarquees = new ArrayList<>();
        for (Cellule cell: getVoisins()) {
            if (! cell.estMarque()) {
                nonMarquees.add(cell);
            }
        }
        if(nonMarquees.isEmpty()) {
            //System.out.println("Cul de sac en " + this.toString());
            return null;
        }
        //System.out.println(tab.size() + " voisins : " + tab);
        int index = RAND.nextInt(nonMarquees.size());
        return nonMarquees.get(index);
    }

    /**
     * Affiche le chemin de l'entrée à une cellule donnée. 
     * Suppose que ce chemin est défini par les valeurs predecesseur de chaque cellule (à partir de la cellule courante)
     * @param cellule la cellule objectif.
     * @param paint la couleur de coloriage.
     */
    public void colorierChemin(Paint paint) {
        Cellule current = this;
        current.colorier(paint);               
        if (this.predecesseur != null) {
            while (current != entree) {
                current = current.predecesseur;
                current.colorier(paint);
            } 
        }
    }

    public void setPredecesseur(Cellule predecesseur) {
        this.predecesseur = predecesseur;
    }
}


