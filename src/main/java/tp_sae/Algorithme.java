/*
 * Copyright (C) 2018 University of Lille
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tp_sae;

/**
 * Classe qui encapsule un alogorithme de resolution de labyrinthe sous javafx.base
 * Délègue un certain nombre de méthodes de la classe Labyrinthe.
 * Ajoute des méthodes outils sous javaFX (sleep).
 * Basée sur la classe tp_sae.Cellule
 */
public abstract class Algorithme extends Thread {
    protected Labyrinthe labyrinthe;

    /**
     * Fournit l'entrée du labyrinthe.
     * @return la Cellule associée à l'entrée.
     */
    public Cellule getEntree() {
        return Cellule.getEntree();
    }

    /**
     * Fournit la sortie du labyrinthe.
     * @return la Cellule associée à la sortie.
     */
    public Cellule getSortie() {
        return Cellule.getSortie();
    }

    /**
     * Cette classe décrit les mouvements possibles. Elle permet donc de calculer l'ensemble des cellules voisines d'une cellule.
     * Chaque mouvement est caractérisé par son déplacement en x et/ou en y (-1, 0 ou +1).
     */
    public static class Move {
        private int x;
        private int y;
        /**
         * Calcule le x pour ce mouvement à partir d'une cellule from.
         * @param from La cellule de départ.
         * @return le x de la cellule cible.
         */
        public int xTarget(Cellule from) { return from.x + this.x;}
        /**
         * Callcule le y pour ce mouvement par rapport à une cellule from.
         * @param from La cellule de départ.
         * @return le y de la cellule cible.
         */
        public int yTarget(Cellule from) { return from.y + this.y;}
        private Move(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    /**
     * Tableau donnant la liste des mouvemets possibles.
     */
    protected static final Move[] MOVES = new Move[] {
            new Move(-1, 0),
            new Move(+1, 0),
            new Move(0, -1),
            new Move(0, +1),
    };

    /**
     * Création de la classe abstraite Algorithme à partir du abyrinthe.
     * @param labyrinthe Le labyrinthe.
     */
    protected Algorithme(Labyrinthe labyrinthe) {
        this.labyrinthe = labyrinthe;
        Cellule.setLabyrinthe(labyrinthe);
        labyrinthe.resetColors();
    }

    /**
     * Teste si un couple de coordonnées (x, y) est valide.
     * @param x le x
     * @param y le y
     * @return vrai si x et y sont entre 0 (inclus) et la taile du labyrinthe (exclu)
     */
    protected boolean isOk(int x, int y) {
        return labyrinthe.isOk(x, y);
    }

    /**
     * Délégation à l'objet labyrinthe (teste si un mur se trouve à une position (x, y)))
     * @param x le x
     * @param y le y
     * @return true si un mur existe à cet endroit
     */
    protected boolean estMur(int x, int y) {
        return labyrinthe.estMur(x, y);
    }

    /**
     * Attente spécifiée en ms/ns
     * @param ms la durée d'attente (partie ms).
     * @param nano la durée d'attente (partie ns)
     */
    protected void pause(long ms, int nano) {
        try {
            Thread.sleep(ms, nano);
        } catch (Exception e) {
            System.out.println("Erreur sur appel tempo");

        }
    }

    /**
     * Attente.
     * @param ms la durée d'attente.
     */
    protected void pause(long ms) {
        pause(ms, 0);
    }

    
    /**
     * Point d'entrée de l'algorithme.
     */
    @Override
    public abstract void run() ;
}
    
