
Ce projet est un squelette de Projet JavaFX permettant de tester différents algorithme de calcul de chemin dans un labyrinthe.

# Elements founis

* Le labyrinthe est dessiné dans la class Labyrinthe sous forme d'un tableau de chaînes de caractère.
Celui-ci a été complété pour pouvoir être dessiné ou mis à jour dans un Pane JavaFX.

* Le Main construit l'IHM qui est en gros la représentation graphique du labyrinthe plus les boutons qui permettent de lancer l'un des 5 algorithme proposés

* La classe Cellule (une cellule représente une case du labyrinthe) dispose au départ:
    * De méthodes permettant d'interroger le labyrinthe (estMur)
    * De primitive de mise en couleur (colorier)
    * On dispose en outre d'une méthode statique permettant de récupérer (ou de créer si besoin) une cellule en position (x, y).

* La classe Algorithme représente un algorithme.    
    * Le lancement du l'algorithme utilise la méthode abstraite run qu'il vaut faudra donc écrire pour chaque algorithme

* En fonction des besoins, pour les algorithmes les plus avancés vous aurez probablement besoin:
    * D'enrichir la classe Cellule avec des nouveaux attributs et/ou méthodes (heuristique, distance, ...)
    * D'utiliser (Pile, File) ou de créer (Dijkstra, A*) es structures de données pertinentes.

La structure du projet (paquetage et fichiers java) est la suivante:

```
tp_sae
   +---- Cellule.java : la classe Cellule (adaptables selon vos besoins)
   |
   +---- Algorithme.java : la classe Algorithme founie (ne pas modifier)
   |
   +---- Labyrinthe : Le dessin et les primitives de base du labyrinthe (ne pas modifier sauf pour changer la topologie)
   |
   +---- Main.java : La classe principale javaFX (ne pas modifier)
   |
   +---- WorkbenchController: le contrôleur javaFX (qui utilise la ressource hello-viw.fxml) ne pas modifier.
   |
   +---+---- tp1 : les classes pour le tp1
   |   |
   |   +---- Demo.java : un exemple d'algorithme (naïf): déplacement complètement aléatoire
   |   |
   |   +---+--- pile: Algorithme pile
   |   |   |  
   |   |   +--- Pile.java:  Prototype d'algorithme à complèter (parcours du labyrinthe en prfondeur à l'aide d'une pile)
   |   |
   |   +---+--- file: Algorithme file
   |       |
   |       +--- File.java:  Prototype d'algorithme à complèter (parcours en largeur à l'aide d'une file)
   | 
   +---+--- dijkstra: Algorithme Dijkstra
   |   |
   |   +--- Dijkstra: Prototype d'algorithme à complèter (algorithme de Dijkstra)
   |   
   +---+--- astar: algorithme A*
       |
       +--- AStar.java: Prototype d'algorithme à complèter (A*)
```

